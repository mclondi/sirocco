package com.mj.sirocco.ui.main

data class TimerState(
    var showTimer: Boolean = false,
    var timer: Long = 0L
)
