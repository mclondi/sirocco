package com.mj.sirocco.ui

import com.mj.sirocco.repostiory.Repository
import com.mj.sirocco.repostiory.RepositoryImpl
import com.mj.sirocco.repostiory.Service
import com.mj.sirocco.ui.main.MainViewModel
import com.mj.sirocco.ui.main.dialog.LoadingDialogViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

val appModule = module {
    single<Service> { get<Retrofit>().create(Service::class.java) }
    single<Repository> { RepositoryImpl(get()) }
    viewModel { MainViewModel(get()) }
    viewModel { LoadingDialogViewModel() }
}