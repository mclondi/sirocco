package com.mj.sirocco.ui.main.recycler

import com.mj.sirocco.repostiory.dto.Item

interface ItemClickListener {
    fun onItemImageClick(item: Item)
}