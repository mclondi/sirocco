package com.mj.sirocco.ui.main

import com.mj.sirocco.repostiory.dto.Item

data class MainState(
    var nameButtonTouched: Boolean = false,
    var items: List<Item> = listOf(),
    var sortEnabled: Boolean = false
)