package com.mj.sirocco.ui.main.recycler

import android.view.View

class OddItemPresentation : ItemPresentationStrategy {

    override val backgroundColorResource: Int = android.R.color.white

    override fun parseTitle(title: String): String = title.toUpperCase()

    override fun itemClicked(view: View) {

    }
}