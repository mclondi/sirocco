package com.mj.sirocco.ui.main.recycler

import android.view.View

class EvenItemPresentation : ItemPresentationStrategy{

    override val backgroundColorResource: Int = android.R.color.holo_blue_light

    override fun parseTitle(title: String): String = title.toLowerCase()

    override fun itemClicked(view: View) {
        view.setBackgroundResource(android.R.color.white)
    }
}