package com.mj.sirocco.ui.splash

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.mj.sirocco.R
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import java.util.concurrent.TimeUnit

class SplashFragment: Fragment(R.layout.fragment_splash) {

    companion object {
        const val SPLASH_SCREEN_DELAY_IN_SECONDS = 1L
    }

    var splashDisposable: Disposable? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        splashDisposable?.dispose()
        splashDisposable = Completable.complete()
            .delay(SPLASH_SCREEN_DELAY_IN_SECONDS, TimeUnit.SECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy {
                goToMainFragment()
            }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        splashDisposable?.dispose()
    }

    private fun goToMainFragment() {
        findNavController().navigate(
            SplashFragmentDirections.actionSplashFragmentToMainFragment()
        )
    }

}