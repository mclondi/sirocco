package com.mj.sirocco.ui.main.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import com.mj.sirocco.R
import kotlinx.android.synthetic.main.dialog_loading.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class LoadingDialog : DialogFragment() {

    companion object {
        fun newInstance(): LoadingDialog {
            return LoadingDialog()
        }
    }

    private val viewModel: LoadingDialogViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_loading, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.progress.observe(this, Observer {
            dialogProgressBar.progress = it
        })
        viewModel.dismiss.observe(this, Observer {
            if (it) {
                dismiss()
            }
        })
    }


}