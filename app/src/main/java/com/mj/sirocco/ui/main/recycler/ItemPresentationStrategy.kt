package com.mj.sirocco.ui.main.recycler

import android.view.View

interface ItemPresentationStrategy {
    val backgroundColorResource: Int
    fun parseTitle(title: String): String
    fun itemClicked(view: View)
}