package com.mj.sirocco.ui.main.recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.mj.sirocco.R
import com.mj.sirocco.repostiory.dto.Item

class ItemAdapter(private val carItemClickListener: ItemClickListener) :
    ListAdapter<Item, ItemViewHolder>(ItemDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_view, parent, false)
        return ItemViewHolder(view, carItemClickListener)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {

        val presentation = if (isPositionEven(position)) {
            EvenItemPresentation()
        } else {
            OddItemPresentation()
        }

        holder.bind(getItem(position), presentation)
    }

    private fun isPositionEven(position: Int) = position > 0 && position.rem(2) == 0

}