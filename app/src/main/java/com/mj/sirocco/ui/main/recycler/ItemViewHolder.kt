package com.mj.sirocco.ui.main.recycler

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mj.sirocco.R
import com.mj.sirocco.repostiory.dto.Item
import kotlinx.android.synthetic.main.item_view.view.*

class ItemViewHolder(itemView: View, private val clickListener: ItemClickListener) :
    RecyclerView.ViewHolder(itemView) {

    fun bind(item: Item, strategy: ItemPresentationStrategy) {
        itemView.itemTitle.text = strategy.parseTitle(item.title)
        itemView.itemView.setBackgroundResource(strategy.backgroundColorResource)

        setOnClickListeners(strategy, item)
        loadImage(item)
    }

    private fun setOnClickListeners(
        strategy: ItemPresentationStrategy,
        item: Item
    ) {
        itemView.itemView.setOnClickListener {
            strategy.itemClicked(it)
        }
        itemView.itemImage.setOnClickListener {
            clickListener.onItemImageClick(item)
        }
    }

    private fun loadImage(item: Item) {
        Glide.with(itemView)
            .load(item.imageUrl)
            .error(R.drawable.logo)
            .centerCrop()
            .into(itemView.itemImage)
    }

}