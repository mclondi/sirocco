package com.mj.sirocco.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mj.sirocco.repostiory.Repository
import com.mj.sirocco.repostiory.dto.Item
import com.mj.sirocco.ui.base.Event
import com.mj.sirocco.ui.main.dialog.DeleteDialogListener
import com.mj.sirocco.ui.main.recycler.ItemClickListener
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import java.util.concurrent.TimeUnit

class MainViewModel(private val repository: Repository) : ViewModel(),
    ItemClickListener,
    DeleteDialogListener {

    companion object {
        const val DATA_LOADING_TIME = 5L
    }

    val viewState = MutableLiveData<MainState>(MainState())
    val timerState = MutableLiveData<TimerState>(TimerState())
    val showProgressDialog = MutableLiveData<Event<Any>>()
    val showDeleteQuestion = MutableLiveData<Event<Item>>()

    private val disposables = CompositeDisposable()
    private var timerDisposable: Disposable? = null
    private var nameButtonClicked = false

    fun userNameClicked() {
        if (!nameButtonClicked) {
            showProgressDialog.value = Event(Any())
        }
    }

    fun userNameTouched() {
        viewState.value = viewState.value?.apply {
            this.nameButtonTouched = true
        }
    }

    fun searchClicked(query: String) {
        if (query.isBlank()) {
            return
        }

        timerDisposable?.dispose()
        timerDisposable = Observable.interval(0L, 1, TimeUnit.SECONDS)
            .take(DATA_LOADING_TIME+1)
            .subscribeBy(onComplete = { fetchItemsFromRepository(query) }
            ) {
                updateTimer(it)
            }
    }

    fun sortClicked() {
        viewState.value = viewState.value?.apply {
            this.items = sortListByIdAndName()
            this.sortEnabled = false
        }
    }

    override fun onItemImageClick(item: Item) {
        showDeleteQuestion.postValue(Event(item))
    }

    override fun onDelete(item: Item) {
        viewState.value = viewState.value?.apply {
            this.items = removeItemFromList(item)
        }
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }

    private fun MainState.sortListByIdAndName(): List<Item> {
        return this.items.sortedWith(
            compareBy({ it.id }, { it.title })
        )
    }

    private fun MainState.removeItemFromList(item: Item): List<Item> {
        return this.items.filter {
            it != item
        }
    }

    private fun fetchItemsFromRepository(query: String) {
        repository.getItems(query)
            .onErrorReturn { listOf() }
            .subscribeBy {
                viewState.value = viewState.value?.apply {
                    this.items = it
                    this.sortEnabled = it.isNotEmpty()
                }
                timerState.value = TimerState()
            }.addTo(disposables)
    }

    private fun updateTimer(secondsPassed: Long) {
        timerState.postValue(timerState.value?.apply {
            this.showTimer = true
            this.timer = DATA_LOADING_TIME - secondsPassed
        })
    }


}
