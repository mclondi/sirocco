package com.mj.sirocco.ui.main.dialog

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import java.util.concurrent.TimeUnit

class LoadingDialogViewModel : ViewModel() {

    companion object {
        const val CLOCK_TICKS = 6L
        const val PROGRESSBAR_FACTOR = 20L
    }


    val progress = MutableLiveData(0)
    val dismiss = MutableLiveData(false)

    private var disposable: Disposable? = null

    init {
        disposable = Flowable.interval(0L, 1, TimeUnit.SECONDS)
            .take(CLOCK_TICKS)
            .subscribeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onComplete = {
                    dismiss.postValue(true)
                }
            ) {
                progress.postValue((it * PROGRESSBAR_FACTOR).toInt())
            }
    }

    override fun onCleared() {
        super.onCleared()
        disposable?.dispose()
    }

}