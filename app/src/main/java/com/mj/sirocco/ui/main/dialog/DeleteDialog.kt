package com.mj.sirocco.ui.main.dialog

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.mj.sirocco.R
import com.mj.sirocco.repostiory.dto.Item


class DeleteDialog : DialogFragment() {

    companion object {
        const val BUNDLE_DELETE_DIALOG = "ITEM"

        fun newInstance(item: Item, listener: DeleteDialogListener): DeleteDialog {
            val dialog = DeleteDialog()
            val bundle = Bundle()
            bundle.putSerializable(BUNDLE_DELETE_DIALOG, item)
            dialog.arguments = bundle
            dialog.listener = listener
            return dialog
        }
    }

    var listener: DeleteDialogListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val item = arguments?.getSerializable(BUNDLE_DELETE_DIALOG) as Item
        return AlertDialog.Builder(context!!)
            .setTitle(getString(R.string.dialog_delete_title, item.title))
            .setPositiveButton(R.string.dialog_delete_yes) { dialog, _ ->
                listener?.onDelete(item)
                dialog.dismiss()
            }
            .setNegativeButton(R.string.dialog_delete_no) { dialog, _ ->
                dialog.dismiss()
            }
            .create()
    }
}