package com.mj.sirocco.ui.main

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.core.widget.TextViewCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.mj.sirocco.R
import com.mj.sirocco.repostiory.dto.Item
import com.mj.sirocco.ui.base.hideKeyboard
import com.mj.sirocco.ui.main.dialog.DeleteDialog
import com.mj.sirocco.ui.main.dialog.LoadingDialog
import com.mj.sirocco.ui.main.recycler.ItemAdapter
import kotlinx.android.synthetic.main.fragment_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainFragment : Fragment(R.layout.fragment_main) {

    companion object {
        const val DELETE_DIALOG_TAG = "deletedialog"
    }

    private val viewModel: MainViewModel by viewModel()

    private lateinit var adapter: ItemAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        attachViewModelToDialogs()
        initRecyclerView()
        initClickListeners()
        initViewModelObservers()
    }

    private fun attachViewModelToDialogs() {
        (fragmentManager?.findFragmentByTag(DELETE_DIALOG_TAG) as DeleteDialog?)?.listener =
            viewModel
    }

    private fun initRecyclerView() {
        adapter = ItemAdapter(viewModel)
        mainRecycler.adapter = adapter
    }

    private fun initClickListeners() {
        nameLabel.setOnTouchListener { _, _ ->
            viewModel.userNameTouched()
            false
        }

        nameLabel.setOnClickListener {
            viewModel.userNameClicked()
        }

        searchButton.setOnClickListener {
            viewModel.searchClicked(queryInput.text.toString())
            context?.hideKeyboard(it)
        }

        sortButton.setOnClickListener {
            viewModel.sortClicked()
        }
    }

    private fun initViewModelObservers() {
        viewModel.showProgressDialog.observe(this, Observer {
            it.getContentIfNotHandled()?.let {
                showLoadingDialog()
            }
        })

        viewModel.showDeleteQuestion.observe(this, Observer {
            it.getContentIfNotHandled()?.let {
                showDeleteDialog(it)
            }
        })

        viewModel.viewState.observe(this, Observer {
            handleTitleTextChange(it.nameButtonTouched)
            sortButton.isEnabled = it.sortEnabled
            adapter.submitList(it.items)
        })

        viewModel.timerState.observe(this, Observer {
            handleTimerChange(it)
        })
    }

    private fun showLoadingDialog() {
        val dialog = LoadingDialog.newInstance()
        dialog.isCancelable = false
        dialog.show(fragmentManager!!, null)
    }

    private fun showDeleteDialog(item: Item) {
        val dialog = DeleteDialog.newInstance(item, viewModel)
        dialog.show(fragmentManager!!, DELETE_DIALOG_TAG)
    }

    private fun handleTitleTextChange(nameButtonTouched: Boolean) {
        val textStyle = if (nameButtonTouched) R.style.Header_Green else R.style.Header_Red
        TextViewCompat.setTextAppearance(nameLabel, textStyle)
    }

    private fun handleTimerChange(state: TimerState) {
        progressGroup.isVisible = state.showTimer
        progressTimer.text = state.timer.toString()
    }

}
