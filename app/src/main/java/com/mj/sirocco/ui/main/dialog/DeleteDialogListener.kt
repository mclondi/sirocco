package com.mj.sirocco.ui.main.dialog

import com.mj.sirocco.repostiory.dto.Item

interface DeleteDialogListener {
    fun onDelete(item: Item)
}