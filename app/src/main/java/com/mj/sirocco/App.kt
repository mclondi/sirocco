package com.mj.sirocco

import android.app.Application
import com.mj.sirocco.repostiory.networkModule
import com.mj.sirocco.ui.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(networkModule)
            modules(appModule)
        }
    }
}