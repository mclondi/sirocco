package com.mj.sirocco.repostiory.dto

import java.io.Serializable

data class Item(
    var id: Long? = null,
    var title: String = "",
    var imageUrl: String = ""
): Serializable