package com.mj.sirocco.repostiory

import com.mj.sirocco.repostiory.dto.Item
import io.reactivex.Scheduler
import io.reactivex.Single

class RepositoryImpl(private val service: Service) : Repository {

    override fun getItems(
        query: String,
        subscribeScheduler: Scheduler,
        observeScheduler: Scheduler
    ): Single<List<Item>> {
        return service.getResponse(query)
            .map { it.data }
            .map { response ->
                response.items.map {
                    Item(
                        it.id,
                        it.title,
                        response.baseUrl.plus(it.url)
                    )
                }
            }
            .observeOn(observeScheduler)
            .subscribeOn(subscribeScheduler)
    }

}