package com.mj.sirocco.repostiory

import com.mj.sirocco.repostiory.dto.Item
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

interface Repository {

    fun getItems(query: String,
                 subscribeScheduler: Scheduler = Schedulers.io(),
                 observeScheduler: Scheduler = AndroidSchedulers.mainThread()
    ): Single<List<Item>>

}