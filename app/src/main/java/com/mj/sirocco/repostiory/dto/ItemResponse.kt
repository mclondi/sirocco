package com.mj.sirocco.repostiory.dto

data class ItemResponse(
    var id: Long? = null,
    var title: String = "",
    var url: String = ""
)