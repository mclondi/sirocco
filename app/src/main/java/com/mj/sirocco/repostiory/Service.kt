package com.mj.sirocco.repostiory

import com.mj.sirocco.repostiory.dto.RequestResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface Service {

    @GET("{search}.json")
    fun getResponse(@Path("search") search: String): Single<RequestResponse>

}