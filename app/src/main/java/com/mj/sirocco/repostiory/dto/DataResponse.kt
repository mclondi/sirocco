package com.mj.sirocco.repostiory.dto

data class DataResponse(
    var baseUrl: String = "",
    var items: List<ItemResponse> = listOf()
)